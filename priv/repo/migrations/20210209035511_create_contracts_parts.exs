defmodule Contractex.Repo.Migrations.CreateContractsParts do
  use Ecto.Migration

  def change do
    create table(:contracts_parts) do
      add :contract_id, references(:contracts, on_delete: :delete_all, type: :binary_id)
      add :part_id, references(:parts, on_delete: :delete_all, type: :binary_id)
    end

    create unique_index(:contracts_parts, [:contract_id, :part_id])
  end
end
