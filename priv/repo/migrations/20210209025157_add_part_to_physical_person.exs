defmodule Contractex.Repo.Migrations.AddPartToPhysicalPerson do
  use Ecto.Migration

  def change do
    alter table(:physicals) do
      add :part_id, references(:parts, on_delete: :nothing, type: :binary_id)
    end

    create unique_index(:physicals, [:part_id])
  end
end
