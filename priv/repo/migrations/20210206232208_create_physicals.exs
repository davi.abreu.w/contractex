defmodule Contractex.Repo.Migrations.CreatePhysicals do
  use Ecto.Migration

  def change do
    create table(:physicals, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :binary, null: false
      add :cpf, :binary, null: false
      add :cpf_hash, :binary
      add :birthdate, :binary, null: false

      timestamps()
    end

    create unique_index(:physicals, [:cpf_hash])
  end
end
