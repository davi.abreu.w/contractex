defmodule Contractex.Repo.Migrations.CreateAddresses do
  use Ecto.Migration

  def change do
    create table(:addresses, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :country, :string, null: false
      add :uf, :string, null: false
      add :city, :string, null: false
      add :street, :string, null: false
      add :number, :string, null: false
      add :zipcode, :string, null: false

      timestamps()
    end
  end
end
