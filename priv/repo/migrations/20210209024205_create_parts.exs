defmodule Contractex.Repo.Migrations.CreateParts do
  use Ecto.Migration

  def change do
    create table(:parts, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :type, :integer, null: false

      timestamps()
    end
  end
end
