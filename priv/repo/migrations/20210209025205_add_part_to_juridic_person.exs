defmodule Contractex.Repo.Migrations.AddPartToJuridicPerson do
  use Ecto.Migration

  def change do
    alter table(:juridics) do
      add :part_id, references(:parts, on_delete: :nothing, type: :binary_id)
    end

    create unique_index(:juridics, [:part_id])
  end
end
