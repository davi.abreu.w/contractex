defmodule Contractex.Repo.Migrations.CreateJuridics do
  use Ecto.Migration

  def change do
    create table(:juridics, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :binary, null: false
      add :cnpj, :binary, null: false
      add :cnpj_hash, :binary
      add :address_id, references(:addresses, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create unique_index(:juridics, [:cnpj_hash])
    create index(:juridics, [:address_id])
  end
end
