defmodule Contractex.Repo do
  use Ecto.Repo,
    otp_app: :contractex,
    adapter: Ecto.Adapters.Postgres
end
