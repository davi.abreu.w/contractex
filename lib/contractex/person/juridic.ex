defmodule Contractex.Person.Juridic do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset
  import Brcpfcnpj.Changeset

  alias Contractex.Addresses.Address
  alias Contractex.Contracts.Part
  alias Contractex.Encrypted

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "juridics" do
    field :cnpj, Encrypted.Binary
    field :cnpj_hash, Cloak.Ecto.SHA256
    field :name, Encrypted.Binary

    belongs_to :address, Address
    belongs_to :part, Part

    timestamps()
  end

  @required_fields [:name, :cnpj]
  @optional_fields [:address_id]

  defp put_hashed_fields(changeset) do
    changeset
    |> put_change(:cnpj_hash, get_field(changeset, :cnpj))
  end

  @doc false
  def changeset(juridic, %{address: _} = attrs) do
    juridic
    |> cast(attrs, @required_fields)
    |> put_hashed_fields()
    |> validate_required(@required_fields)
    |> validate_cnpj(:cnpj)
    |> unique_constraint([:cnpj_hash])
    |> cast_assoc(:part, required: true)
    |> cast_assoc(:address, required: true)
  end

  def changeset(juridic, attrs) do
    juridic
    |> cast(attrs, @required_fields ++ @optional_fields)
    |> put_hashed_fields()
    |> validate_required(@required_fields ++ @optional_fields)
    |> validate_cnpj(:cnpj)
    |> unique_constraint([:cnpj_hash])
    |> cast_assoc(:part, required: true)
  end
end
