defmodule Contractex.Person.Physical do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset
  import Brcpfcnpj.Changeset

  alias Contractex.Contracts.Part
  alias Contractex.Encrypted

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "physicals" do
    field :birthdate, Encrypted.Date
    field :cpf, Encrypted.Binary
    field :cpf_hash, Cloak.Ecto.SHA256
    field :name, Encrypted.Binary

    belongs_to :part, Part
    timestamps()
  end

  defp put_hashed_fields(changeset) do
    changeset
    |> put_change(:cpf_hash, get_field(changeset, :cpf))
  end

  @required_fields [:name, :cpf, :birthdate]
  @doc false
  def changeset(physical, attrs) do
    physical
    |> cast(attrs, @required_fields)
    |> put_hashed_fields()
    |> validate_required(@required_fields)
    |> validate_cpf(:cpf)
    |> unique_constraint([:cpf_hash])
    |> cast_assoc(:part, required: true)
  end
end
