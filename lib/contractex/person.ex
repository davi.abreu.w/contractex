defmodule Contractex.Person do
  @moduledoc """
  The Person context.
  """

  import Ecto.Query, warn: false
  alias Contractex.Repo

  alias Contractex.Person.Physical

  @doc """
  Returns the list of physicals.

  ## Examples

      iex> list_physicals()
      [%Physical{}, ...]

  """
  def list_physicals do
    Physical
    |> Repo.all()
    |> Repo.preload(:part)
  end

  @doc """
  Gets a single physical.

  ## Examples

      iex> get_physical(123)
      {:ok, %Physical{}}

      iex> get_physical(456)
      {:error, :not_found}

  """
  def get_physical(id) do
    case Repo.get(Physical, id) do
      %Physical{} = physical -> {:ok, Repo.preload(physical, :part)}
      nil -> {:error, %{id: :not_found}}
    end
  rescue
    Ecto.Query.CastError -> {:error, %{id: :not_found}}
  end

  @doc """
  Creates a physical.

  ## Examples

      iex> create_physical(%{field: value})
      {:ok, %Physical{}}

      iex> create_physical(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_physical(attrs \\ %{}) do
    attrs = Map.put(attrs, :part, %{type: :person_physical})

    %Physical{}
    |> Physical.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a physical.

  ## Examples

      iex> update_physical(physical, %{field: new_value})
      {:ok, %Physical{}}

      iex> update_physical(physical, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_physical(%Physical{} = physical, attrs) do
    physical
    |> Physical.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a physical.

  ## Examples

      iex> delete_physical(physical)
      {:ok, %Physical{}}

      iex> delete_physical(physical)
      {:error, %Ecto.Changeset{}}

  """
  def delete_physical(%Physical{} = physical) do
    Repo.delete(physical)
  end

  alias Contractex.Person.Juridic

  @doc """
  Returns the list of juridics.

  ## Examples

      iex> list_juridics()
      [%Juridic{}, ...]

  """
  def list_juridics do
    Juridic
    |> Repo.all()
    |> Repo.preload([:address, :part])
  end

  @doc """
  Gets a single juridic.

  ## Examples

      iex> get_juridic(123)
      {:ok, %Juridic{}}

      iex> get_juridic(456)
      {:error, :not_found}

  """
  def get_juridic(id) do
    case Repo.get(Juridic, id) do
      %Juridic{} = juridic -> {:ok, Repo.preload(juridic, [:address, :part])}
      nil -> {:error, %{id: :not_found}}
    end
  rescue
    Ecto.Query.CastError -> {:error, %{id: :not_found}}
  end

  @doc """
  Creates a juridic.

  ## Examples

      iex> create_juridic(%{field: value})
      {:ok, %Juridic{}}

      iex> create_juridic(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_juridic(attrs \\ %{}) do
    attrs = Map.put(attrs, :part, %{type: :person_juridic})

    %Juridic{}
    |> Juridic.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a juridic.

  ## Examples

      iex> update_juridic(juridic, %{field: new_value})
      {:ok, %Juridic{}}

      iex> update_juridic(juridic, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_juridic(%Juridic{} = juridic, attrs) do
    juridic
    |> Juridic.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a juridic.

  ## Examples

      iex> delete_juridic(juridic)
      {:ok, %Juridic{}}

      iex> delete_juridic(juridic)
      {:error, %Ecto.Changeset{}}

  """
  def delete_juridic(%Juridic{} = juridic) do
    Repo.delete(juridic)
  end
end
