defmodule Contractex.Contracts do
  @moduledoc """
  The Contracts context.
  """

  import Ecto.Query, warn: false
  alias Contractex.Repo

  alias Contractex.Contracts.Contract
  alias Contractex.Contracts.Part

  def get_part(id) do
    case Repo.get(Part, id) do
      %Part{} = part -> {:ok, part}
      nil -> {:error, %{id: :not_found}}
    end
  rescue
    Ecto.Query.CastError -> {:error, %{id: :not_found}}
  end

  @doc """
  Returns the list of contracts.

  ## Examples

      iex> list_contracts()
      [%Contract{}, ...]

  """
  def list_contracts do
    Contract
    |> Repo.all()
    |> Repo.preload(:parts)
  end

  @doc """
  Gets a single contract.

  ## Examples

      iex> get_contract(123)
      {:ok, %Contract{}}

      iex> get_contract(456)
      {:error, %{id: :not_found}}

  """
  def get_contract(id) do
    case Repo.get(Contract, id) do
      %Contract{} = contract -> {:ok, Repo.preload(contract, :parts)}
      nil -> {:error, %{id: :not_found}}
    end
  rescue
    Ecto.Query.CastError -> {:error, %{id: :not_found}}
  end

  defp reduce_parts(part_ids) when is_list(part_ids) do
    Enum.reduce_while(
      part_ids,
      [],
      fn part_id, acc ->
        case get_part(part_id) do
          {:ok, part} -> {:cont, acc ++ [part]}
          {:error, %{id: :not_found}} -> {:halt, %{id: :not_found}}
        end
      end
    )
  end

  defp get_parts(part_ids) when is_list(part_ids) do
    case reduce_parts(part_ids) do
      parts when is_list(parts) -> {:ok, parts}
      %{id: :not_found} -> {:error, %{id: :not_found}}
    end
  end

  defp load_parts(changeset, part_ids) when is_list(part_ids) do
    case get_parts(part_ids) do
      {:ok, parts} -> Ecto.Changeset.put_assoc(changeset, :parts, parts)
      {:error, %{id: :not_found}} -> Ecto.Changeset.add_error(changeset, :part_id, "not found")
    end
  end

  @doc """
  Creates a contract.

  ## Examples

      iex> create_contract(%{field: value})
      {:ok, %Contract{}}

      iex> create_contract(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_contract(%{part_ids: nil}),
    do:
      {:error,
       %Contract{}
       |> Ecto.Changeset.change()
       |> Ecto.Changeset.add_error(:part_ids, "can't be blank")}

  def create_contract(%{part_ids: []}),
    do:
      {:error,
       %Contract{}
       |> Ecto.Changeset.change()
       |> Ecto.Changeset.add_error(:part_ids, "required at least one")}

  def create_contract(%{part_ids: part_ids} = attrs) when is_list(part_ids) do
    %Contract{}
    |> Contract.changeset(attrs)
    |> load_parts(part_ids)
    |> Repo.insert()
  end

  @doc """
  Updates a contract.

  ## Examples

      iex> update_contract(contract, %{field: new_value})
      {:ok, %Contract{}}

      iex> update_contract(contract, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_contract(%Contract{} = contract, attrs) do
    contract
    |> Contract.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a contract.

  ## Examples

      iex> delete_contract(contract)
      {:ok, %Contract{}}

      iex> delete_contract(contract)
      {:error, %Ecto.Changeset{}}

  """
  def delete_contract(%Contract{} = contract) do
    Repo.delete(contract)
  end
end
