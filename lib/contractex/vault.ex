defmodule Contractex.Vault do
  @moduledoc """
  Requirement for cloak setup
  """
  use Cloak.Vault, otp_app: :contractex
end
