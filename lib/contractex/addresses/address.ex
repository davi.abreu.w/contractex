defmodule Contractex.Addresses.Address do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  alias Contractex.Person.Juridic

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "addresses" do
    field :city, :string
    field :country, :string
    field :number, :string
    field :street, :string
    field :uf, :string
    field :zipcode, :string

    has_one :juridic, Juridic

    timestamps()
  end

  @required_fields [:country, :uf, :city, :street, :number, :zipcode]

  @doc false
  def changeset(address, attrs) do
    address
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
  end
end
