defmodule Contractex.Contracts.Contract do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  alias Contractex.Contracts.Part

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "contracts" do
    field :description, :string
    field :name, :string

    many_to_many :parts, Part, join_through: "contracts_parts"

    timestamps()
  end

  @required_fields [:name, :description]
  @doc false
  def changeset(contract, attrs) do
    contract
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
  end
end
