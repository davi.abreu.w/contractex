defmodule Contractex.Contracts.Part do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset

  alias Contractex.Contracts.Contract
  alias Contractex.Person.Juridic
  alias Contractex.Person.Physical

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "parts" do
    field :type, PartType

    has_one :juridic, Juridic
    has_one :physical, Physical
    many_to_many :contracts, Contract, join_through: "contracts_parts"

    timestamps()
  end

  @required_fields [:type]
  @doc false
  def changeset(part, attrs) do
    part
    |> cast(attrs, @required_fields)
    |> validate_required(@required_fields)
  end
end
