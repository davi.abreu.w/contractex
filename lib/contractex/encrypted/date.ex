defmodule Contractex.Encrypted.Date do
  @moduledoc """
  Type for date Database encryption
  """

  use Cloak.Ecto.Date, vault: Contractex.Vault
end
