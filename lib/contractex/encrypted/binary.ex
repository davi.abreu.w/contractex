defmodule Contractex.Encrypted.Binary do
  @moduledoc """
  Type for string Database encryption
  """

  use Cloak.Ecto.Binary, vault: Contractex.Vault
end
