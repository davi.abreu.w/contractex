defmodule ContractexWeb.Schema.ContractSchema do
  @moduledoc """
  Type Contract for Graphql
  """

  use Absinthe.Schema.Notation

  import ContractexWeb.ErrorHelpers

  alias Contractex.Contracts

  object :contract do
    field :id, :id
    field :name, :string
    field :description, :string
    field :inserted_at, :naive_datetime
    field :updated_at, :naive_datetime
  end

  object :contract_queries do
    @desc "Retrieve all Contracts"
    field :contracts, list_of(:contract) do
      resolve(fn _parent, _args, _resolution ->
        {:ok, Contracts.list_contracts()}
      end)
    end

    @desc "Retrieve a Contract"
    field :contract, :contract do
      arg(:id, non_null(:string))

      resolve(fn _parent, %{id: id}, _resolution ->
        case Contracts.get_contract(id) do
          {:ok, contract} -> {:ok, contract}
          {:error, error} -> {:error, translate_errors(error)}
        end
      end)
    end
  end

  object :contract_mutations do
    @desc "Create a new Contract"
    field :create_contract, :contract do
      arg(:description, non_null(:string))
      arg(:name, non_null(:string))
      arg(:part_ids, non_null(list_of(:string)))

      resolve(fn _parent, args, _resolution ->
        case Contracts.create_contract(args) do
          {:ok, contract} -> {:ok, contract}
          {:error, error} -> {:error, translate_errors(error)}
        end
      end)
    end

    @desc "Update an existing Contract"
    field :update_contract, :contract do
      arg(:id, non_null(:string))
      arg(:description, non_null(:string))
      arg(:name, non_null(:string))
      arg(:part_ids, non_null(list_of(:string)))

      resolve(fn _parent, %{id: id} = args, _resolution ->
        case Contracts.get_contract(id) do
          {:ok, contract} ->
            case Contracts.update_contract(contract, args) do
              {:ok, contract} -> {:ok, contract}
              {:error, error} -> {:error, translate_errors(error)}
            end

          {:error, error} ->
            {:error, translate_errors(error)}
        end
      end)
    end

    @desc "Delete an existing Contract"
    field :delete_contract, :contract do
      arg(:id, non_null(:string))

      resolve(fn _parent, %{id: id}, _resolution ->
        case Contracts.get_contract(id) do
          {:ok, contract} ->
            case Contracts.delete_contract(contract) do
              {:ok, contract} -> {:ok, contract}
              {:error, error} -> {:error, translate_errors(error)}
            end

          {:error, error} ->
            {:error, translate_errors(error)}
        end
      end)
    end
  end
end
