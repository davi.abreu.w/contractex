defmodule ContractexWeb.Schema.PersonJuridicSchema do
  @moduledoc """
  Type PersonJuridic for Graphql
  """

  use Absinthe.Schema.Notation

  import ContractexWeb.ErrorHelpers

  alias Contractex.Person

  object :juridic do
    field :id, :id
    field :name, :string
    field :cnpj, :string
    field :address_id, :id
    field :part_id, :id
    field :inserted_at, :naive_datetime
    field :updated_at, :naive_datetime
  end

  object :juridic_queries do
    @desc "Retrieve all Juridic Persons"
    field :juridic_persons, list_of(:juridic) do
      resolve(fn _parent, _args, _resolution ->
        {:ok, Person.list_juridics()}
      end)
    end

    @desc "Retrieve a Juridic Person"
    field :juridic_person, :juridic do
      arg(:id, non_null(:string))

      resolve(fn _parent, %{id: id}, _resolution ->
        case Person.get_juridic(id) do
          {:ok, juridic} -> {:ok, juridic}
          {:error, error} -> {:error, translate_errors(error)}
        end
      end)
    end
  end

  object :juridic_mutations do
    @desc "Create a new Juridic Person"
    field :create_juridic_person, :juridic do
      arg(:name, non_null(:string))
      arg(:cnpj, non_null(:string))
      arg(:address_id, :string)
      arg(:address, :input_address)

      resolve(fn _parent, args, _resolution ->
        case Person.create_juridic(args) do
          {:ok, juridic} -> {:ok, juridic}
          {:error, error} -> {:error, translate_errors(error)}
        end
      end)
    end

    @desc "Update an existing Juridic Person"
    field :update_juridic_person, :juridic do
      arg(:id, non_null(:string))
      arg(:name, non_null(:string))
      arg(:cnpj, non_null(:string))
      arg(:address_id, non_null(:string))

      resolve(fn _parent, %{id: id} = args, _resolution ->
        case Person.get_juridic(id) do
          {:ok, juridic} ->
            case Person.update_juridic(juridic, args) do
              {:ok, juridic} -> {:ok, juridic}
              {:error, error} -> {:error, translate_errors(error)}
            end

          {:error, error} ->
            {:error, translate_errors(error)}
        end
      end)
    end

    @desc "Delete an existing Juridic Person"
    field :delete_juridic_person, :juridic do
      arg(:id, non_null(:string))

      resolve(fn _parent, %{id: id}, _resolution ->
        case Person.get_juridic(id) do
          {:ok, juridic} ->
            case Person.delete_juridic(juridic) do
              {:ok, juridic} -> {:ok, juridic}
              {:error, error} -> {:error, translate_errors(error)}
            end

          {:error, error} ->
            {:error, translate_errors(error)}
        end
      end)
    end
  end
end
