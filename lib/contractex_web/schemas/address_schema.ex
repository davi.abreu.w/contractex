defmodule ContractexWeb.Schema.AddressSchema do
  @moduledoc """
  Type Address for Graphql
  """

  use Absinthe.Schema.Notation

  import ContractexWeb.ErrorHelpers

  alias Contractex.Addresses

  object :address do
    field :id, :id
    field :city, :string
    field :country, :string
    field :number, :string
    field :street, :string
    field :uf, :string
    field :zipcode, :string
    field :inserted_at, :naive_datetime
    field :updated_at, :naive_datetime
  end

  input_object :input_address do
    field :city, :string
    field :country, :string
    field :number, :string
    field :street, :string
    field :uf, :string
    field :zipcode, :string
  end

  object :address_queries do
    @desc "Retrieve all Addresses"
    field :addresses, list_of(:address) do
      resolve(fn _parent, _args, _resolution ->
        {:ok, Addresses.list_addresses()}
      end)
    end

    @desc "Retrieve an Address"
    field :address, :address do
      arg(:id, non_null(:string))

      resolve(fn _parent, %{id: id}, _resolution ->
        case Addresses.get_address(id) do
          {:ok, address} -> {:ok, address}
          {:error, error} -> {:error, translate_errors(error)}
        end
      end)
    end
  end

  object :address_mutations do
    @desc "Create a new Address"
    field :create_address, :address do
      arg(:city, non_null(:string))
      arg(:country, non_null(:string))
      arg(:number, non_null(:string))
      arg(:street, non_null(:string))
      arg(:uf, non_null(:string))
      arg(:zipcode, non_null(:string))

      resolve(fn _parent, args, _resolution ->
        case Addresses.create_address(args) do
          {:ok, address} -> {:ok, address}
          {:error, error} -> {:error, translate_errors(error)}
        end
      end)
    end

    @desc "Update an existing Address"
    field :update_address, :address do
      arg(:id, non_null(:string))
      arg(:city, non_null(:string))
      arg(:country, non_null(:string))
      arg(:number, non_null(:string))
      arg(:street, non_null(:string))
      arg(:uf, non_null(:string))
      arg(:zipcode, non_null(:string))

      resolve(fn _parent, %{id: id} = args, _resolution ->
        case Addresses.get_address(id) do
          {:ok, address} ->
            case Addresses.update_address(address, args) do
              {:ok, address} -> {:ok, address}
              {:error, error} -> {:error, translate_errors(error)}
            end

          {:error, error} ->
            {:error, translate_errors(error)}
        end
      end)
    end

    @desc "Delete an existing Address"
    field :delete_address, :address do
      arg(:id, non_null(:string))

      resolve(fn _parent, %{id: id}, _resolution ->
        case Addresses.get_address(id) do
          {:ok, address} ->
            case Addresses.delete_address(address) do
              {:ok, address} -> {:ok, address}
              {:error, error} -> {:error, translate_errors(error)}
            end

          {:error, error} ->
            {:error, translate_errors(error)}
        end
      end)
    end
  end
end
