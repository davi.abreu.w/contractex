defmodule ContractexWeb.Schema.PersonPhysicalSchema do
  @moduledoc """
  Type PersonPhysical for Graphql
  """

  use Absinthe.Schema.Notation

  import ContractexWeb.ErrorHelpers

  alias Contractex.Person

  object :physical do
    field :id, :id
    field :name, :string
    field :cpf, :string
    field :birthdate, :string
    field :part_id, :id
    field :inserted_at, :naive_datetime
    field :updated_at, :naive_datetime
  end

  object :physical_queries do
    @desc "Retrieve all Physical Persons"
    field :physical_persons, list_of(:physical) do
      resolve(fn _parent, _args, _resolution ->
        {:ok, Person.list_physicals()}
      end)
    end

    @desc "Retrieve a Physical Person"
    field :physical_person, :physical do
      arg(:id, non_null(:string))

      resolve(fn _parent, %{id: id}, _resolution ->
        case Person.get_physical(id) do
          {:ok, physical} -> {:ok, physical}
          {:error, error} -> {:error, translate_errors(error)}
        end
      end)
    end
  end

  object :physical_mutations do
    @desc "Create a new Physical Person"
    field :create_physical_person, :physical do
      arg(:name, non_null(:string))
      arg(:cpf, non_null(:string))
      arg(:birthdate, non_null(:string))

      resolve(fn _parent, args, _resolution ->
        case Person.create_physical(args) do
          {:ok, physical} -> {:ok, physical}
          {:error, error} -> {:error, translate_errors(error)}
        end
      end)
    end

    @desc "Update an existing Physical Person"
    field :update_physical_person, :physical do
      arg(:id, non_null(:string))
      arg(:name, non_null(:string))
      arg(:cpf, non_null(:string))
      arg(:birthdate, non_null(:string))

      resolve(fn _parent, %{id: id} = args, _resolution ->
        case Person.get_physical(id) do
          {:ok, physical} ->
            case Person.update_physical(physical, args) do
              {:ok, physical} -> {:ok, physical}
              {:error, error} -> {:error, translate_errors(error)}
            end

          {:error, error} ->
            {:error, translate_errors(error)}
        end
      end)
    end

    @desc "Delete an existing Physical Person"
    field :delete_physical_person, :physical do
      arg(:id, non_null(:string))

      resolve(fn _parent, %{id: id}, _resolution ->
        case Person.get_physical(id) do
          {:ok, physical} ->
            case Person.delete_physical(physical) do
              {:ok, physical} -> {:ok, physical}
              {:error, error} -> {:error, translate_errors(error)}
            end

          {:error, error} ->
            {:error, translate_errors(error)}
        end
      end)
    end
  end
end
