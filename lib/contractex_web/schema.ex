defmodule ContractexWeb.Schema do
  @moduledoc """
  Handler for Graphql
  """

  use Absinthe.Schema

  import_types(Absinthe.Type.Custom)

  import_types(ContractexWeb.Schema.AddressSchema)
  import_types(ContractexWeb.Schema.ContractSchema)
  import_types(ContractexWeb.Schema.PersonPhysicalSchema)
  import_types(ContractexWeb.Schema.PersonJuridicSchema)

  query do
    import_fields(:address_queries)
    import_fields(:contract_queries)
    import_fields(:physical_queries)
    import_fields(:juridic_queries)
  end

  mutation do
    import_fields(:address_mutations)
    import_fields(:contract_mutations)
    import_fields(:physical_mutations)
    import_fields(:juridic_mutations)
  end
end
