defmodule Contractex.Schemas.AddressFixture do
  @moduledoc """
  Graphql helper.
  """

  def addresses_query do
    """
    query {
      addresses {
        id
        city
        country
        number
        street
        uf
        zipcode

        insertedAt
        updatedAt
      }
    }
    """
  end

  def address_query(id) do
    """
    query {
      address(id: "#{id}") {
        id
        city
        country
        number
        street
        uf
        zipcode


        insertedAt
        updatedAt
      }
    }
    """
  end

  def create_address_mutation(%{
        city: city,
        country: country,
        number: number,
        street: street,
        uf: uf,
        zipcode: zipcode
      }) do
    """
    mutation {
      createAddress(
        city: "#{city}",
        country: "#{country}",
        number: "#{number}",
        street: "#{street}",
        uf: "#{uf}",
        zipcode: "#{zipcode}"
      ), {
        id
        city
        country
        number
        street
        uf
        zipcode

        insertedAt
        updatedAt
      }
    }
    """
  end

  def update_address_mutation(%{
        id: id,
        city: city,
        country: country,
        number: number,
        street: street,
        uf: uf,
        zipcode: zipcode
      }) do
    """
    mutation {
      updateAddress(
        id: "#{id}",
        city: "#{city}",
        country: "#{country}",
        number: "#{number}",
        street: "#{street}",
        uf: "#{uf}",
        zipcode: "#{zipcode}"
      ), {
        id
        city
        country
        number
        street
        uf
        zipcode

        insertedAt
        updatedAt
      }
    }
    """
  end

  def delete_address_mutation(id) do
    """
    mutation {
      deleteAddress(
        id: "#{id}"
      ), {
        id
        city
        country
        number
        street
        uf
        zipcode

        insertedAt
        updatedAt
      }
    }
    """
  end
end
