defmodule Contractex.Schemas.ContractFixture do
  @moduledoc """
  Graphql helper.
  """

  def response do
    """
    id
    description
    name
    insertedAt
    updatedAt
    """
  end

  def contracts_query do
    """
    query {
      contracts {
        #{response()}
      }
    }
    """
  end

  def contract_query(id) do
    """
    query {
      contract(id: "#{id}") {
        #{response()}
      }
    }
    """
  end

  def create_contract_mutation(%{
        description: description,
        name: name,
        part_ids: part_ids
      }) do
    """
    mutation {
      createContract(
        description: "#{description}",
        name: "#{name}",
        part_ids: ["#{part_ids}"]
      ), {
        id
        #{response()}
      }
    }
    """
  end

  def update_contract_mutation(%{
        id: id,
        description: description,
        name: name,
        part_ids: part_ids
      }) do
    """
    mutation {
      updateContract(
        id: "#{id}",
        description: "#{description}",
        name: "#{name}",
        part_ids: ["#{part_ids}"]
      ), {
        #{response()}
      }
    }
    """
  end

  def delete_contract_mutation(id) do
    """
    mutation {
      deleteContract(
        id: "#{id}"
      ), {
        #{response()}
      }
    }
    """
  end
end
