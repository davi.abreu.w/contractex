defmodule Contractex.Schemas.PersonJuridicFixture do
  @moduledoc """
  Graphql helper.
  """

  defp response do
    """
    id
    name
    cnpj
    address_id
    part_id
    insertedAt
    updatedAt
    """
  end

  def juridic_persons_query do
    """
    query {
      juridicPersons {
        #{response()}
      }
    }
    """
  end

  def juridic_person_query(id) do
    """
    query {
      juridicPerson(id: "#{id}") {
        #{response()}
      }
    }
    """
  end

  def create_juridic_person_mutation(%{name: name, cnpj: cnpj, address_id: address_id}) do
    """
    mutation {
      createJuridicPerson(
        name: "#{name}",
        cnpj: "#{cnpj}",
        address_id: "#{address_id}"
      ), {
        #{response()}
      }
    }
    """
  end

  def create_juridic_person_mutation(%{name: name, cnpj: cnpj, address: address}) do
    """
    mutation {
      createJuridicPerson(
        name: "#{name}",
        cnpj: "#{cnpj}",
        address: {
          city: "#{address.city}",
          country: "#{address.country}",
          number: "#{address.number}",
          street: "#{address.street}",
          uf: "#{address.uf}",
          zipcode: "#{address.zipcode}"
        }
      ), {
        #{response()}
      }
    }
    """
  end

  def update_juridic_person_mutation(%{id: id, name: name, cnpj: cnpj, address_id: address_id}) do
    """
    mutation {
      updateJuridicPerson(
        id: "#{id}"
        name: "#{name}",
        cnpj: "#{cnpj}",
        address_id: "#{address_id}"
      ), {
        #{response()}
      }
    }
    """
  end

  def delete_juridic_person_mutation(id) do
    """
    mutation {
      deleteJuridicPerson(
        id: "#{id}"
      ), {
        #{response()}
      }
    }
    """
  end
end
