defmodule Contractex.Schemas.PersonPhysicalFixture do
  @moduledoc """
  Graphql helper.
  """

  defp response do
    """
    id
    name
    cpf
    birthdate
    part_id
    insertedAt
    updatedAt
    """
  end

  def physical_persons_query do
    """
    query {
      physicalPersons {
        #{response()}
      }
    }
    """
  end

  def physical_person_query(id) do
    """
    query {
      physicalPerson(id: "#{id}") {
        #{response()}
      }
    }
    """
  end

  def create_physical_person_mutation(%{name: name, cpf: cpf, birthdate: birthdate}) do
    """
    mutation {
      createPhysicalPerson(
        name: "#{name}",
        cpf: "#{cpf}",
        birthdate: "#{birthdate}"
      ), {
        #{response()}
      }
    }
    """
  end

  def update_physical_person_mutation(%{id: id, name: name, cpf: cpf, birthdate: birthdate}) do
    """
    mutation {
      updatePhysicalPerson(
        id: "#{id}"
        name: "#{name}",
        cpf: "#{cpf}",
        birthdate: "#{birthdate}"
      ), {
        #{response()}
      }
    }
    """
  end

  def delete_physical_person_mutation(id) do
    """
    mutation {
      deletePhysicalPerson(
        id: "#{id}"
      ), {
        #{response()}
      }
    }
    """
  end
end
