defmodule ContractexWeb.Schema.AddressSchemaTest do
  use ContractexWeb.ConnCase

  import Contractex.Factory
  import Contractex.Schemas.AddressFixture

  alias Contractex.Graphql

  describe "addresses" do
    test "list all addresses", %{conn: conn} do
      address = insert(:address)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => addresses_query()
        })

      [response | _] = json_response(conn, 200)["data"]["addresses"]

      assert address.id == response["id"]
      assert address.city == response["city"]
      assert address.country == response["country"]
      assert address.number == response["number"]
      assert address.street == response["street"]
      assert address.uf == response["uf"]
      assert address.zipcode == response["zipcode"]
    end
  end

  describe "address" do
    test "get a address", %{conn: conn} do
      address = insert(:address)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => address_query(address.id)
        })

      response = json_response(conn, 200)["data"]["address"]

      assert address.id == response["id"]
      assert address.city == response["city"]
      assert address.country == response["country"]
      assert address.number == response["number"]
      assert address.street == response["street"]
      assert address.uf == response["uf"]
      assert address.zipcode == response["zipcode"]
    end

    test "not found error", %{conn: conn} do
      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => address_query("404")
        })

      assert List.first(json_response(conn, 200)["errors"])["message"] == %{"id" => "not_found"}
    end
  end

  describe "createAddress" do
    test "create a new address", %{conn: conn} do
      attrs = params_for(:address)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => create_address_mutation(attrs)
        })

      response = json_response(conn, 200)["data"]["createAddress"]

      assert is_binary(response["id"])
      assert attrs.city == response["city"]
      assert attrs.country == response["country"]
      assert attrs.number == response["number"]
      assert attrs.street == response["street"]
      assert attrs.uf == response["uf"]
      assert attrs.zipcode == response["zipcode"]
    end
  end

  describe "updateAddress" do
    test "update a address", %{conn: conn} do
      address = insert(:address)

      attrs =
        :address
        |> params_for()
        |> Map.put(:id, address.id)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => update_address_mutation(attrs)
        })

      response = json_response(conn, 200)["data"]["updateAddress"]

      assert is_binary(response["id"])
      assert attrs.city == response["city"]
      assert attrs.country == response["country"]
      assert attrs.number == response["number"]
      assert attrs.street == response["street"]
      assert attrs.uf == response["uf"]
      assert attrs.zipcode == response["zipcode"]
    end

    test "not found error", %{conn: conn} do
      attrs =
        :address
        |> params_for()
        |> Map.put(:id, Ecto.UUID.generate())

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => update_address_mutation(attrs)
        })

      assert List.first(json_response(conn, 200)["errors"])["message"] == %{"id" => "not_found"}
    end
  end

  describe "deleteAddress" do
    test "delete a address", %{conn: conn} do
      address = insert(:address)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => delete_address_mutation(address.id)
        })

      response = json_response(conn, 200)["data"]["deleteAddress"]

      assert is_binary(response["id"])
      assert address.city == response["city"]
      assert address.country == response["country"]
      assert address.number == response["number"]
      assert address.street == response["street"]
      assert address.uf == response["uf"]
      assert address.zipcode == response["zipcode"]
    end

    test "not found error", %{conn: conn} do
      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => delete_address_mutation("404")
        })

      assert List.first(json_response(conn, 200)["errors"])["message"] == %{"id" => "not_found"}
    end
  end
end
