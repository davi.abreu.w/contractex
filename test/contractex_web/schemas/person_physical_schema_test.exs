defmodule ContractexWeb.Schema.PersonPhysicalSchemaTest do
  use ContractexWeb.ConnCase

  import Contractex.Factory
  import Contractex.Schemas.PersonPhysicalFixture

  alias Contractex.Graphql

  describe "physicalPersons" do
    test "list all physical persons", %{conn: conn} do
      physical = insert(:person_physical)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => physical_persons_query()
        })

      [response | _] = json_response(conn, 200)["data"]["physicalPersons"]

      assert physical.id == response["id"]
      assert physical.name == response["name"]
      assert physical.cpf == response["cpf"]
      assert Date.to_iso8601(physical.birthdate) == response["birthdate"]
      assert physical.part_id == response["part_id"]
    end
  end

  describe "physicalPerson" do
    test "get a physical person", %{conn: conn} do
      physical = insert(:person_physical)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => physical_person_query(physical.id)
        })

      response = json_response(conn, 200)["data"]["physicalPerson"]

      assert physical.id == response["id"]
      assert physical.name == response["name"]
      assert physical.cpf == response["cpf"]
      assert Date.to_iso8601(physical.birthdate) == response["birthdate"]
      assert physical.part_id == response["part_id"]
    end

    test "not found error", %{conn: conn} do
      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => physical_person_query("404")
        })

      assert List.first(json_response(conn, 200)["errors"])["message"] == %{"id" => "not_found"}
    end
  end

  describe "createPhysicalPerson" do
    test "create a new physical person", %{conn: conn} do
      attrs = params_for(:person_physical)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => create_physical_person_mutation(attrs)
        })

      response = json_response(conn, 200)["data"]["createPhysicalPerson"]

      assert is_binary(response["id"])
      assert attrs.name == response["name"]
      assert attrs.cpf == response["cpf"]
      assert Date.to_iso8601(attrs.birthdate) == response["birthdate"]
      assert is_binary(response["part_id"])
    end

    test "duplicate cpf error", %{conn: conn} do
      attrs = params_for(:person_physical)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => create_physical_person_mutation(attrs)
        })

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => create_physical_person_mutation(attrs)
        })

      assert List.first(json_response(conn, 200)["errors"])["message"] == %{
               "cpf_hash" => ["has already been taken"]
             }
    end
  end

  describe "updatePhysicalPerson" do
    test "update a physical person", %{conn: conn} do
      physical = insert(:person_physical)

      attrs =
        :person_physical
        |> params_for()
        |> Map.put(:id, physical.id)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => update_physical_person_mutation(attrs)
        })

      response = json_response(conn, 200)["data"]["updatePhysicalPerson"]

      assert physical.id == response["id"]
      assert attrs.name == response["name"]
      assert attrs.cpf == response["cpf"]
      assert Date.to_iso8601(attrs.birthdate) == response["birthdate"]
      assert physical.part_id == response["part_id"]
    end

    test "not found error", %{conn: conn} do
      attrs =
        :person_physical
        |> params_for()
        |> Map.put(:id, Ecto.UUID.generate())

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => update_physical_person_mutation(attrs)
        })

      assert List.first(json_response(conn, 200)["errors"])["message"] == %{"id" => "not_found"}
    end
  end

  describe "deletePhysicalPerson" do
    test "delete a physical person", %{conn: conn} do
      physical = insert(:person_physical)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => delete_physical_person_mutation(physical.id)
        })

      response = json_response(conn, 200)["data"]["deletePhysicalPerson"]

      assert physical.id == response["id"]
      assert physical.name == response["name"]
      assert physical.cpf == response["cpf"]
      assert Date.to_iso8601(physical.birthdate) == response["birthdate"]
      assert physical.part_id == response["part_id"]
    end

    test "not found error", %{conn: conn} do
      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => delete_physical_person_mutation("404")
        })

      assert List.first(json_response(conn, 200)["errors"])["message"] == %{"id" => "not_found"}
    end
  end
end
