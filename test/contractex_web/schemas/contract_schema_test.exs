defmodule ContractexWeb.Schema.ContractSchemaTest do
  use ContractexWeb.ConnCase

  import Contractex.Factory
  import Contractex.Schemas.ContractFixture

  alias Contractex.Graphql

  describe "contracts" do
    test "list all contracts", %{conn: conn} do
      contract = insert(:contract)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => contracts_query()
        })

      [response | _] = json_response(conn, 200)["data"]["contracts"]

      assert contract.id == response["id"]
      assert contract.description == response["description"]
      assert contract.name == response["name"]
    end
  end

  describe "contract" do
    test "get a contract", %{conn: conn} do
      contract = insert(:contract)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => contract_query(contract.id)
        })

      response = json_response(conn, 200)["data"]["contract"]

      assert contract.id == response["id"]
      assert contract.description == response["description"]
      assert contract.name == response["name"]
    end

    test "not found error", %{conn: conn} do
      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => contract_query("404")
        })

      assert List.first(json_response(conn, 200)["errors"])["message"] == %{"id" => "not_found"}
    end
  end

  describe "createContract" do
    test "create a new contract", %{conn: conn} do
      part = insert(:person_physical).part

      attrs =
        :contract
        |> params_for()
        |> Map.put(:part_ids, [part.id])

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => create_contract_mutation(attrs)
        })

      response = json_response(conn, 200)["data"]["createContract"]

      assert is_binary(response["id"])
      assert attrs.description == response["description"]
      assert attrs.name == response["name"]
    end
  end

  describe "updateContract" do
    test "update a contract", %{conn: conn} do
      contract = insert(:contract)

      attrs =
        :contract
        |> params_for()
        |> Map.put(:id, contract.id)
        |> Map.put(:part_ids, [List.first(contract.parts).id])

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => update_contract_mutation(attrs)
        })

      response = json_response(conn, 200)["data"]["updateContract"]

      assert contract.id == response["id"]
      assert attrs.description == response["description"]
      assert attrs.name == response["name"]
    end

    test "not found error", %{conn: conn} do
      part = insert(:person_physical).part

      attrs =
        :contract
        |> params_for()
        |> Map.put(:id, Ecto.UUID.generate())
        |> Map.put(:part_ids, [part.id])

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => update_contract_mutation(attrs)
        })

      assert List.first(json_response(conn, 200)["errors"])["message"] == %{"id" => "not_found"}
    end
  end

  describe "deleteContract" do
    test "delete a contract", %{conn: conn} do
      contract = insert(:contract)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => delete_contract_mutation(contract.id)
        })

      response = json_response(conn, 200)["data"]["deleteContract"]

      assert contract.id == response["id"]
      assert contract.description == response["description"]
      assert contract.name == response["name"]
    end

    test "not found error", %{conn: conn} do
      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => delete_contract_mutation("404")
        })

      assert List.first(json_response(conn, 200)["errors"])["message"] == %{"id" => "not_found"}
    end
  end
end
