defmodule ContractexWeb.Schema.PersonJuridicSchemaTest do
  use ContractexWeb.ConnCase

  import Contractex.Factory
  import Contractex.Schemas.PersonJuridicFixture

  alias Contractex.Graphql

  describe "juridicPersons" do
    test "list all juridic persons", %{conn: conn} do
      juridic = insert(:person_juridic)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => juridic_persons_query()
        })

      [response | _] = json_response(conn, 200)["data"]["juridicPersons"]

      assert juridic.id == response["id"]
      assert juridic.name == response["name"]
      assert juridic.cnpj == response["cnpj"]
      assert juridic.address_id == response["address_id"]
      assert juridic.part_id == response["part_id"]
    end
  end

  describe "juridicPerson" do
    test "get a juridic person", %{conn: conn} do
      juridic = insert(:person_juridic)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => juridic_person_query(juridic.id)
        })

      response = json_response(conn, 200)["data"]["juridicPerson"]

      assert juridic.id == response["id"]
      assert juridic.name == response["name"]
      assert juridic.cnpj == response["cnpj"]
      assert juridic.address_id == response["address_id"]
      assert juridic.part_id == response["part_id"]
    end

    test "not found error", %{conn: conn} do
      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => juridic_person_query("404")
        })

      assert List.first(json_response(conn, 200)["errors"])["message"] == %{"id" => "not_found"}
    end
  end

  describe "createJuridicPerson" do
    test "create a new juridic person", %{conn: conn} do
      attrs = params_with_assocs(:person_juridic)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => create_juridic_person_mutation(attrs)
        })

      response = json_response(conn, 200)["data"]["createJuridicPerson"]

      assert is_binary(response["id"])
      assert attrs.name == response["name"]
      assert attrs.cnpj == response["cnpj"]
      assert attrs.address_id == response["address_id"]
      assert is_binary(response["part_id"])
    end

    test "create a new juridic person with address", %{conn: conn} do
      attrs =
        :person_juridic
        |> params_for()
        |> Map.put(:address, params_for(:address))

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => create_juridic_person_mutation(attrs)
        })

      response = json_response(conn, 200)["data"]["createJuridicPerson"]

      assert is_binary(response["id"])
      assert attrs.name == response["name"]
      assert attrs.cnpj == response["cnpj"]
      assert is_binary(response["address_id"])
    end

    test "duplicate cnpj error", %{conn: conn} do
      attrs = params_with_assocs(:person_juridic)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => create_juridic_person_mutation(attrs)
        })

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => create_juridic_person_mutation(attrs)
        })

      assert List.first(json_response(conn, 200)["errors"])["message"] == %{
               "cnpj_hash" => ["has already been taken"]
             }
    end
  end

  describe "updateJuridicPerson" do
    test "update a juridic person", %{conn: conn} do
      juridic = insert(:person_juridic)

      attrs =
        :person_juridic
        |> params_for()
        |> Map.put(:id, juridic.id)
        |> Map.put(:address_id, juridic.address.id)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => update_juridic_person_mutation(attrs)
        })

      response = json_response(conn, 200)["data"]["updateJuridicPerson"]

      assert attrs.name == response["name"]
      assert attrs.cnpj == response["cnpj"]
      assert attrs.address_id == response["address_id"]
      assert juridic.part_id == response["part_id"]
    end

    test "not found error", %{conn: conn} do
      attrs =
        :person_juridic
        |> params_for()
        |> Map.put(:id, Ecto.UUID.generate())
        |> Map.put(:address_id, Ecto.UUID.generate())

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => update_juridic_person_mutation(attrs)
        })

      assert List.first(json_response(conn, 200)["errors"])["message"] == %{"id" => "not_found"}
    end
  end

  describe "deleteJuridicPerson" do
    test "delete a juridic person", %{conn: conn} do
      juridic = insert(:person_juridic)

      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => delete_juridic_person_mutation(juridic.id)
        })

      response = json_response(conn, 200)["data"]["deleteJuridicPerson"]

      assert juridic.id == response["id"]
      assert juridic.name == response["name"]
      assert juridic.cnpj == response["cnpj"]
      assert juridic.address_id == response["address_id"]
      assert juridic.part_id == response["part_id"]
    end

    test "not found error", %{conn: conn} do
      conn =
        post(conn, Graphql.endpoint(), %{
          "query" => delete_juridic_person_mutation("404")
        })

      assert List.first(json_response(conn, 200)["errors"])["message"] == %{"id" => "not_found"}
    end
  end
end
