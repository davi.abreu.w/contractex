defmodule Contractex.AddressesTest do
  use Contractex.DataCase

  import Contractex.Factory

  alias Contractex.Addresses

  describe "addresses" do
    alias Contractex.Addresses.Address

    @invalid_attrs %{
      cep: nil,
      city: nil,
      country: nil,
      number: nil,
      street: nil,
      uf: nil,
      zipcode: nil
    }

    test "list_addresses/0 returns all addresses" do
      address = insert(:address)
      assert Addresses.list_addresses() == [address]
    end

    test "get_address/1 returns the address with given id" do
      address = insert(:address)
      assert {:ok, ^address} = Addresses.get_address(address.id)
    end

    test "get_address/1 returns not found" do
      assert {:error, %{id: :not_found}} == Addresses.get_address("404")
    end

    test "create_address/1 with valid data creates a address" do
      attrs = params_for(:address)
      assert {:ok, %Address{} = address} = Addresses.create_address(attrs)
      assert address.city == attrs.city
      assert address.country == attrs.country
      assert address.number == attrs.number
      assert address.street == attrs.street
      assert address.uf == attrs.uf
      assert address.zipcode == attrs.zipcode
    end

    test "create_address/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Addresses.create_address(@invalid_attrs)
    end

    test "update_address/2 with valid data updates the address" do
      address = insert(:address)
      attrs = params_for(:address)
      assert {:ok, %Address{} = address} = Addresses.update_address(address, attrs)
      assert address.city == attrs.city
      assert address.country == attrs.country
      assert address.number == attrs.number
      assert address.street == attrs.street
      assert address.uf == attrs.uf
      assert address.zipcode == attrs.zipcode
    end

    test "update_address/2 with invalid data returns error changeset" do
      address = insert(:address)
      assert {:error, %Ecto.Changeset{}} = Addresses.update_address(address, @invalid_attrs)
      assert {:ok, ^address} = Addresses.get_address(address.id)
    end

    test "delete_address/1 deletes the address" do
      address = insert(:address)
      assert {:ok, %Address{}} = Addresses.delete_address(address)
      assert {:error, %{id: :not_found}} == Addresses.get_address(address.id)
    end
  end
end
