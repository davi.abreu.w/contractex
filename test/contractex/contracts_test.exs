defmodule Contractex.ContractsTest do
  use Contractex.DataCase

  import Contractex.Factory

  alias Contractex.Contracts

  describe "parts" do
    test "get_part/1 returns the contract with given id" do
      part = insert(:part)

      assert {:ok, ^part} = Contracts.get_part(part.id)
    end
  end

  describe "contracts" do
    alias Contractex.Contracts.Contract

    @invalid_attrs %{description: nil, name: nil, part_ids: []}

    test "list_contracts/0 returns all contracts" do
      contract = insert(:contract)

      assert Contracts.list_contracts() == [contract]
    end

    test "get_contract/1 returns the contract with given id" do
      contract = insert(:contract)

      assert {:ok, ^contract} = Contracts.get_contract(contract.id)
    end

    test "create_contract/1 with valid data creates a contract" do
      part = insert(:person_physical).part

      attrs =
        :contract
        |> params_for()
        |> Map.put(:part_ids, [part.id])

      assert {:ok, %Contract{} = contract} = Contracts.create_contract(attrs)
      assert contract.description == attrs.description
      assert contract.name == attrs.name
      assert List.first(contract.parts) == part
    end

    test "create_contract/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Contracts.create_contract(@invalid_attrs)
    end

    test "update_contract/2 with valid data updates the contract" do
      contract = insert(:contract)
      attrs = params_for(:contract)

      assert {:ok, %Contract{} = contract} = Contracts.update_contract(contract, attrs)
      assert contract.description == attrs.description
      assert contract.name == attrs.name
    end

    test "update_contract/2 with invalid data returns error changeset" do
      contract = insert(:contract)

      assert {:error, %Ecto.Changeset{}} = Contracts.update_contract(contract, @invalid_attrs)
      assert {:ok, ^contract} = Contracts.get_contract(contract.id)
    end

    test "delete_contract/1 deletes the contract" do
      contract = insert(:contract)

      assert {:ok, %Contract{}} = Contracts.delete_contract(contract)
      assert {:error, %{id: :not_found}} == Contracts.get_contract(contract.id)
    end
  end
end
