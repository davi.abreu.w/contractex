defmodule Contractex.PersonTest do
  use Contractex.DataCase

  import Contractex.Factory

  alias Contractex.Person

  describe "physicals" do
    alias Contractex.Person.Physical

    test "list_physicals/0 returns all physicals" do
      physical = insert(:person_physical)

      assert Person.list_physicals() == [physical]
    end

    test "get_physical/1 returns the physical with given id" do
      physical = insert(:person_physical)

      assert {:ok, ^physical} = Person.get_physical(physical.id)
    end

    test "get_physical/1 returns not found" do
      assert {:error, %{id: :not_found}} == Person.get_physical(Ecto.UUID.generate())
    end

    test "create_physical/1 with valid data creates a physical" do
      attrs = params_for(:person_physical)

      assert {:ok, %Physical{} = physical} = Person.create_physical(attrs)
      assert physical.birthdate == attrs.birthdate
      assert physical.cpf == attrs.cpf
      assert physical.name == attrs.name
      assert physical.part.type == :person_physical
    end

    test "create_physical/1 with invalid cpf returns error Invalid Cpf" do
      attrs = params_for(:person_physical, cpf: "123")

      assert {:error, %Ecto.Changeset{} = changeset} = Person.create_physical(attrs)
      assert errors_on(changeset) == %{cpf: ["Invalid Cpf"]}
    end

    test "create_physical/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Person.create_physical(%{})
    end

    test "update_physical/2 with valid data updates the physical" do
      physical = insert(:person_physical)
      attrs = params_for(:person_physical)

      assert {:ok, %Physical{} = physical} = Person.update_physical(physical, attrs)
      assert physical.birthdate == attrs.birthdate
      assert physical.cpf == attrs.cpf
      assert physical.name == attrs.name
    end

    test "update_physical/2 with invalid data returns error changeset" do
      physical = insert(:person_physical)

      assert {:error, %Ecto.Changeset{}} = Person.update_physical(physical, %{name: 123})
      assert {:ok, physical} == Person.get_physical(physical.id)
    end

    test "delete_physical/1 deletes the physical" do
      physical = insert(:person_physical)

      assert {:ok, %Physical{}} = Person.delete_physical(physical)
      assert {:error, %{id: :not_found}} = Person.get_physical(physical.id)
    end
  end

  describe "juridics" do
    alias Contractex.Person.Juridic

    @invalid_attrs %{cnpj: "123", name: nil}

    test "list_juridics/0 returns all juridics" do
      juridic = insert(:person_juridic)

      assert Person.list_juridics() == [juridic]
    end

    test "get_juridic/1 returns the juridic with given id" do
      juridic = insert(:person_juridic)

      assert {:ok, ^juridic} = Person.get_juridic(juridic.id)
    end

    test "get_juridic/1 returns not found" do
      assert {:error, %{id: :not_found}} == Person.get_juridic(Ecto.UUID.generate())
    end

    test "create_juridic/1 with address_id creates a juridic" do
      attrs = params_with_assocs(:person_juridic)

      assert {:ok, %Juridic{} = juridic} = Person.create_juridic(attrs)
      assert juridic.cnpj == attrs.cnpj
      assert juridic.name == attrs.name
      assert juridic.address_id == attrs.address_id
      assert juridic.part.type == :person_juridic
    end

    test "create_juridic/1 without address_id returns error" do
      attrs = params_for(:person_juridic)

      assert {:error, changeset} = Person.create_juridic(attrs)
      assert errors_on(changeset) == %{address_id: ["can't be blank"]}
    end

    test "create_juridic/1 with address params creates a juridic and an address" do
      %{address: attrs_address} =
        attrs =
        :person_juridic
        |> params_for()
        |> Map.put(:address, params_for(:address))

      assert {:ok, %Juridic{address: address} = juridic} = Person.create_juridic(attrs)
      assert juridic.cnpj == attrs.cnpj
      assert juridic.name == attrs.name
      assert juridic.part.type == :person_juridic
      assert is_binary(juridic.address_id)
      assert address.city == attrs_address.city
      assert address.country == attrs_address.country
      assert address.number == attrs_address.number
      assert address.street == attrs_address.street
      assert address.uf == attrs_address.uf
      assert address.zipcode == attrs_address.zipcode
    end

    test "create_juridic/1 with invalid address returns error" do
      attrs =
        :person_juridic
        |> params_for()
        |> Map.put(:address, %{})

      assert {:error, changeset} = Person.create_juridic(attrs)

      assert errors_on(changeset) == %{
               address: %{
                 city: ["can't be blank"],
                 country: ["can't be blank"],
                 number: ["can't be blank"],
                 street: ["can't be blank"],
                 uf: ["can't be blank"],
                 zipcode: ["can't be blank"]
               }
             }
    end

    test "create_juridic/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Person.create_juridic(@invalid_attrs)
    end

    test "update_juridic/2 with valid data updates the juridic" do
      juridic = insert(:person_juridic)
      attrs = params_for(:person_juridic)

      assert {:ok, %Juridic{} = juridic} = Person.update_juridic(juridic, attrs)
      assert juridic.cnpj == attrs.cnpj
      assert juridic.name == attrs.name
    end

    test "update_juridic/2 with invalid data returns error changeset" do
      juridic = insert(:person_juridic)

      assert {:error, %Ecto.Changeset{}} = Person.update_juridic(juridic, @invalid_attrs)
      assert {:ok, ^juridic} = Person.get_juridic(juridic.id)
    end

    test "delete_juridic/1 deletes the juridic" do
      juridic = insert(:person_juridic)

      assert {:ok, %Juridic{}} = Person.delete_juridic(juridic)
      assert {:error, %{id: :not_found}} == Person.get_juridic(juridic.id)
    end
  end
end
