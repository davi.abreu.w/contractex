defmodule Contractex.PersonPhysicalFactory do
  @moduledoc false

  alias Contractex.Person.Physical
  alias Faker
  alias Faker.Person

  defmacro __using__(_opts) do
    quote do
      def person_physical_factory do
        %Physical{
          birthdate: Faker.Date.date_of_birth(),
          name: Person.PtBr.name(),
          cpf: Brcpfcnpj.cpf_generate(true),
          part: build(:part, type: :person_juridic)
        }
      end
    end
  end
end
