defmodule Contractex.ContractFactory do
  @moduledoc false

  alias Contractex.Contracts.Contract
  alias Faker.Cat
  alias Faker.Person.PtBr

  defmacro __using__(_opts) do
    quote do
      def contract_factory do
        %Contract{
          name: PtBr.name(),
          description: Cat.registry(),
          parts: [build(:person_physical).part]
        }
      end
    end
  end
end
