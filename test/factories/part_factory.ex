defmodule Contractex.PartFactory do
  @moduledoc false

  alias Contractex.Contracts.Part

  defmacro __using__(_opts) do
    quote do
      def part_factory do
        %Part{
          type: :person_physical
        }
      end
    end
  end
end
