defmodule Contractex.PersonJuridicFactory do
  @moduledoc false

  alias Contractex.Person.Juridic
  alias Faker.Cat

  defmacro __using__(_opts) do
    quote do
      def person_juridic_factory do
        %Juridic{
          name: Cat.registry(),
          cnpj: Brcpfcnpj.cnpj_generate(true),
          address: build(:address),
          part: build(:part, type: :person_juridic)
        }
      end
    end
  end
end
