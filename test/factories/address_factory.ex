defmodule Contractex.AddressFactory do
  @moduledoc false

  alias Contractex.Addresses.Address
  alias Faker.Address.PtBr

  defmacro __using__(_opts) do
    quote do
      def address_factory do
        %Address{
          city: PtBr.city(),
          country: PtBr.country(),
          number: PtBr.building_number(),
          street: PtBr.street_name(),
          uf: PtBr.state_abbr(),
          zipcode: PtBr.zip_code()
        }
      end
    end
  end
end
