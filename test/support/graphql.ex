defmodule Contractex.Graphql do
  @moduledoc """
  Graphql helper.
  """

  def endpoint(), do: "/api/graphql"
end
