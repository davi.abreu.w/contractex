defmodule Contractex.Factory do
  @moduledoc """
  Configure test factories.
  """

  use ExMachina.Ecto, repo: Contractex.Repo

  use Contractex.AddressFactory
  use Contractex.ContractFactory
  use Contractex.PartFactory
  use Contractex.PersonJuridicFactory
  use Contractex.PersonPhysicalFactory
end
