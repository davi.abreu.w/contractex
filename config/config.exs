# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :contractex,
  ecto_repos: [Contractex.Repo],
  generators: [binary_id: true]

# Cloak init
config :contractex, Contractex.Vault,
  ciphers: [
    default:
      {Cloak.Ciphers.AES.GCM,
       tag: "AES.GCM.V1", key: Base.decode64!("CC12s3zRDfT2cWolesqXkztUDj1KWWaOZ1qVEwMQCZ4=")}
  ]

# Configures the endpoint
config :contractex, ContractexWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "JY9I53DvlS0DypoKqjGXOJAPEVU8im3eCTO8N3NaLjn8Hn05Ej/bgOOJXyQLeZYq",
  render_errors: [view: ContractexWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: Contractex.PubSub,
  live_view: [signing_salt: "2dz0k6PU"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
